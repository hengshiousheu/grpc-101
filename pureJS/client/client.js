var grpc = require('grpc')
var greets = require('../server/protos/greet_pb')
var services = require('../server/protos/greet_grpc_pb')

var calculators = require('../server/protos/calculator_pb')
var calculatorServices = require('../server/protos/calculator_grpc_pb')

function simpleGreetclient(client){

    var request = new greets.GreetRequest()
    var greeting = new greets.Greeting//object
    greeting.setFirstName("Jerry")
    greeting.setLastName("Tom")

    request.setGreeting(greeting)
    client.greet(request, (error, response) => {
        if(!error){
            console.log("Greeting Resonse:", response.getResult())
        }else {
            console.log(error)
        }
    })
}

function OneManyGreetManyTimes(client){
    var greetManyTimesRequest = new greets.GreetManyTimesRequest()
    var greetingManyTimes = new greets.Greeting()
    greetingManyTimes.setFirstName('Paulo')
    greetingManyTimes.setLastName('dichone')
    greetManyTimesRequest.setGreeting(greetingManyTimes)
    var call = client.greetManyTimes(greetManyTimesRequest, () => {})
    call.on('data', (response) => {
        console.log('client Streaming REsponse: ', response.getResult());
    })
    call.on('status', (status) => {
        console.log(status);
    })
    call.on('error', (error) => {
        console.error(error);
    })
    call.on('end', () => {
        console.log('Streaming Ended!');
    })
}

function calSum(calculatorClient){
    var sumRequest = new calculators.AggregationRequest()// take services.methods from greet.proto
    var Aggregation = new calculators.Aggregation//object
    Aggregation.setFirstNum(1)
    Aggregation.setSecondNum(9)
    sumRequest.setInputnum(Aggregation)
    //set 2 Int32 numbers
    calculatorClient.sumInt(sumRequest, (error, response) => {
        if(!error){
            console.log("Call method successfully, the default result: ", response.getResult())
        }else {
            console.log(error)
        }
    })
}

function calPrintPrime(calculatorClient){
    var printPrimeNumberRequest = new calculators.PrintPrimeNumberRequest()
    var printPrimeNumber = new calculators.Aggregation()
    printPrimeNumber.setPrimeNum(120)
    printPrimeNumberRequest.setInputnum(printPrimeNumber)
    var primeCall = calculatorClient.printPrimeNumber(printPrimeNumberRequest, () => {})
    primeCall.on('data', (response) => {
        console.log('client Streaming Prime number Response: ', response.getResult());
    })
    primeCall.on('status', (status) => {
        console.log(status);
    })
    primeCall.on('error', (error) => {
        console.error(error);
    })
    primeCall.on('end', () => {
        console.log('Streaming Ended!');
    })
}

function callLongGreeting(client){
    var request = new greets.LongGreetRequest()
    var call = client.longGreet(request, (error, response) => {
        if(!error) {
            console.log('server Response: ', response.getResult())
        }
        else {
            console.error(error)
        }
    })
    let count = 0, intervalID = setInterval(function() {
        console.log('Sending message ' + count)

        var request = new greets.LongGreetRequest()
        var greeting = new greets.Greeting()
        greeting.setFirstName('Hulu')
        greeting.setLastName('Netflix')
        request.setLongGreeting(greeting)

        var anotherRequest = new greets.LongGreetRequest()
        var anotherGreetingObj = new greets.Greeting()
        anotherGreetingObj.setFirstName('Another')
        anotherGreetingObj.setLastName('Obj name')
        anotherRequest.setLongGreeting(anotherGreetingObj)

        call.write(request)
        call.write(anotherRequest)
        if(++count > 3){
            clearInterval(intervalID)
            call.end()
        }
    }, 1000)
}

function calStreamingAvg(calculatorClient){

    //Define Request
    var request = new calculators.ComputeAvgRequest()
    var call = calculatorClient.computeAvg(request, (error, response) => {
        if(!error) {
            console.log('server Response: ', response.getResult())
        }
        else {
            console.error(error)
        }
    })

    const list = [1,2,3,4];
    list.forEach(element => {
        var computeAvgRequest = new calculators.ComputeAvgRequest()
        var aggregation = new calculators.Aggregation()
        aggregation.setComputeAvgNum(element)
        computeAvgRequest.setInputnum(aggregation)
        call.write(computeAvgRequest)
    });
    call.end()
}

async function sleep(interval) {
    return new Promise( (resolve) => {
        setTimeout(() => resolve(), interval)
    })
}
async function callBiDirect(client){
    var call = client.greetEveryone(request, (error, response) => {
        console.log('Server Response: '+ response)
    })
    call.on('data',response => {
        console.log('Hello Client! ' + response.getResult())
    })
    call.on('error', error => {
        console.error(error)
    })
    call.on('end', () => {
        console.log('Client The End')
    })

    for (let index = 0; index < 10; index++) {
        var greeting = new greets.Greeting()
        greeting.setFirstName('Stephane')
        greeting.setLastName('Maarek')

        var request = new greets.GreetEveryoneRequest()
        request.setGreeting(greeting)

        call.write(request)
        await sleep(1500)
        
    }
    call.end()
}

function main() {
    console.log("Hello From client")
    var client = new services.GreetServiceClient(
        'localhost:50051',
        grpc.credentials.createInsecure()
    )
    console.log("Info from client: " , client)

    //
    var calculatorClient = new calculatorServices.CalculatorServiceClient(
        'localhost:50051',
        grpc.credentials.createInsecure()
    )
    console.log("Info from client: " , calculatorClient)

    simpleGreetclient(client)
    //OneManyGreetManyTimes(client)
    //calSum(calculatorClient)
    //calPrintPrime(calculatorClient)
    //callLongGreeting(client)
    //calStreamingAvg(calculatorClient)
    //callBiDirect(client)
}

main()