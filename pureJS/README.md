# JavaScript Version 
本資料夾存在 JS 版本之 gRPC toy example
## 使用說明

安裝該安裝的
```
npm install
```

編譯 *.proto
```
cd ../protos
grpc_tools_node_protoc --js_out=import_style=commonjs,binary:../pureJS/server/protos/ \
--grpc_out=../pureJS/server/protos/ \
--plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` \
greet.proto"
```

開啟兩個 terminal，一個使用 server ，一個使用 client
```
cd server && node index.js
```
```
cd client && node client.js
```

## 未來改進
此版本為 statically generate protobuffer
之後需要另外使用 dynamically 版本，npm package 為[@grpc/proto-loader](https://www.npmjs.com/package/@grpc/proto-loader)