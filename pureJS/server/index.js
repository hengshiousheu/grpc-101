var grpc = require('grpc')
var greets = require('../server/protos/greet_pb')
var services = require('../server/protos/greet_grpc_pb')

var calculators = require('../server/protos/calculator_pb')
var calculatorServices = require('../server/protos/calculator_grpc_pb')

// Implements the great RPC method.
function greet(call, callback){
    var greetingResponse = new greets.GreetResponse()

    greetingResponse.setResult(
        "[Javascript]Hello " + call.request.getGreeting().getFirstName()
    )

    callback(null, greetingResponse)
}

function greetManyTimesImpl(call, callback){

    var firstName = call.request.getGreeting().getFirstName()

    let count = 0, intervalID = setInterval( function() {
        var greetManyTimesResponse = new greets.GreetManyTimesResponse()
        greetManyTimesResponse.setResult(
            firstName
        )

        //setup streaming
        call.write(greetManyTimesResponse)
        if(++count > 9){
            clearInterval(intervalID)
            call.end()
        }
    }, 1000)
}

function sumIntImpl(call, callback) {
    var aggregationResponse = new calculators.AggregationResponse()

    aggregationResponse.setResult(
        call.request.getInputnum().getFirstNum() + call.request.getInputnum().getSecondNum() 
    )

    callback(null, aggregationResponse)

}

function printPrimeNumberResponseImpl(call, callback) {

    var primeNumber = call.request.getInputnum().getPrimeNum()

    k = 2
    while( primeNumber > 1){
        if(primeNumber % k ==0){// if k evenly divides into N
            var printPrimeNumberResponse = new calculators.PrintPrimeNumberResponse()
            printPrimeNumberResponse.setResult(k)
            call.write(printPrimeNumberResponse)
            primeNumber = primeNumber / k    // divide N by k so that we have the rest of the number left.
        }else {
            k = k + 1
        }
    }
    call.end()
}

function longGreetImpl(call, callback) {
    call.on('data', request => {
        var fullName = request.getLongGreeting().getFirstName() + ' ' + request.getLongGreeting().getLastName()

        console.log('Hello ' + fullName)
    })
    call.on('error', err => {
        console.error(error)
    })
    call.on('end', () => {
        var response = new greets.LongGreetResponse()
        response.setResult('Long Greet Client Streaming......')

        callback(null, response)
    })
}

function ComputeAvgImpl(call, callback){
    var requestInputNumList = []
    call.on('data', request => {
        var requestInputNum = request.getInputnum().getComputeAvgNum();
        requestInputNumList.push(requestInputNum)
        console.log('Received number: ', requestInputNum)
    })
    call.on('error', err=>{
        console.error(error)
    })
    call.on('end', () => {
        var response = new calculators.ComputeAvgResponse()
        const average = list => list.reduce((prev, curr) => prev + curr) / list.length;
        response.setResult(average(requestInputNumList))
        callback(null, response)
    })
}

async function sleep(interval) {
    return new Promise( (resolve) => {
        setTimeout(() => resolve(), interval)
    })
}
async function greetEveryoneImpl(call, callback) {
    call.on('data', response => {
        var fullName = response.getGreeting().getFirstName() + ' ' + response.getGreeting().getLastName()
        console.log('Hello ' + fullName)
    })
    call.on('error', error => {
        console.error(error)
    })
    call.on('end', () => {
        console.log('The End...')
    })

    for (let index = 0; index < 10; index++) {
        var greeting = new greets.Greeting()
        greeting.setFirstName('Dice')
        greeting.setLastName('Nickles')
        
        var request = new greets.GreetEveryoneResponse()
        request.setResult('Dice' + ' ' + 'Nickles')
        call.write(request)

        await sleep(interval = 1000)
    }
    call.end()
}

function main() {
    var server = new grpc.Server()

    const localhost = "localhost:50051"
    server.addService(services.GreetServiceService, {
        greet: greet,
        greetManyTimes: greetManyTimesImpl,
        longGreet: longGreetImpl,
        greetEveryone: greetEveryoneImpl
    })
    server.addService(calculatorServices.CalculatorServiceService, {
        sumInt: sumIntImpl,
        printPrimeNumber: printPrimeNumberResponseImpl,
        computeAvg: ComputeAvgImpl
    })
    server.bind(localhost, grpc.ServerCredentials.createInsecure())
    server.start()

    console.log('server running on ', localhost)

}

main()