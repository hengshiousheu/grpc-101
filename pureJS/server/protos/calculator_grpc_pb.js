// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var protos_calculator_pb = require('../protos/calculator_pb.js');

function serialize_calculator_AggregationRequest(arg) {
  if (!(arg instanceof protos_calculator_pb.AggregationRequest)) {
    throw new Error('Expected argument of type calculator.AggregationRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_calculator_AggregationRequest(buffer_arg) {
  return protos_calculator_pb.AggregationRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_calculator_AggregationResponse(arg) {
  if (!(arg instanceof protos_calculator_pb.AggregationResponse)) {
    throw new Error('Expected argument of type calculator.AggregationResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_calculator_AggregationResponse(buffer_arg) {
  return protos_calculator_pb.AggregationResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_calculator_ComputeAvgRequest(arg) {
  if (!(arg instanceof protos_calculator_pb.ComputeAvgRequest)) {
    throw new Error('Expected argument of type calculator.ComputeAvgRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_calculator_ComputeAvgRequest(buffer_arg) {
  return protos_calculator_pb.ComputeAvgRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_calculator_ComputeAvgResponse(arg) {
  if (!(arg instanceof protos_calculator_pb.ComputeAvgResponse)) {
    throw new Error('Expected argument of type calculator.ComputeAvgResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_calculator_ComputeAvgResponse(buffer_arg) {
  return protos_calculator_pb.ComputeAvgResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_calculator_PrintPrimeNumberRequest(arg) {
  if (!(arg instanceof protos_calculator_pb.PrintPrimeNumberRequest)) {
    throw new Error('Expected argument of type calculator.PrintPrimeNumberRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_calculator_PrintPrimeNumberRequest(buffer_arg) {
  return protos_calculator_pb.PrintPrimeNumberRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_calculator_PrintPrimeNumberResponse(arg) {
  if (!(arg instanceof protos_calculator_pb.PrintPrimeNumberResponse)) {
    throw new Error('Expected argument of type calculator.PrintPrimeNumberResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_calculator_PrintPrimeNumberResponse(buffer_arg) {
  return protos_calculator_pb.PrintPrimeNumberResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var CalculatorServiceService = exports.CalculatorServiceService = {
  // Unary API
sumInt: {
    path: '/calculator.CalculatorService/SumInt',
    requestStream: false,
    responseStream: false,
    requestType: protos_calculator_pb.AggregationRequest,
    responseType: protos_calculator_pb.AggregationResponse,
    requestSerialize: serialize_calculator_AggregationRequest,
    requestDeserialize: deserialize_calculator_AggregationRequest,
    responseSerialize: serialize_calculator_AggregationResponse,
    responseDeserialize: deserialize_calculator_AggregationResponse,
  },
  // one-many streaming API
printPrimeNumber: {
    path: '/calculator.CalculatorService/PrintPrimeNumber',
    requestStream: false,
    responseStream: true,
    requestType: protos_calculator_pb.PrintPrimeNumberRequest,
    responseType: protos_calculator_pb.PrintPrimeNumberResponse,
    requestSerialize: serialize_calculator_PrintPrimeNumberRequest,
    requestDeserialize: deserialize_calculator_PrintPrimeNumberRequest,
    responseSerialize: serialize_calculator_PrintPrimeNumberResponse,
    responseDeserialize: deserialize_calculator_PrintPrimeNumberResponse,
  },
  // Client(Many-One) Streaming API
computeAvg: {
    path: '/calculator.CalculatorService/ComputeAvg',
    requestStream: true,
    responseStream: false,
    requestType: protos_calculator_pb.ComputeAvgRequest,
    responseType: protos_calculator_pb.ComputeAvgResponse,
    requestSerialize: serialize_calculator_ComputeAvgRequest,
    requestDeserialize: deserialize_calculator_ComputeAvgRequest,
    responseSerialize: serialize_calculator_ComputeAvgResponse,
    responseDeserialize: deserialize_calculator_ComputeAvgResponse,
  },
};

exports.CalculatorServiceClient = grpc.makeGenericClientConstructor(CalculatorServiceService);
