# proto 
## 內容說明
該資料夾存在 *.proto 來建立 toy example

- dummy.proto: 沒用的東西跟名字一樣
- greet.proto: 用來示範對傳 文字資訊
- calculator.proto: 用來對傳需要計算用的 services

## 改進地方
之後需使用 [style guide](https://developers.google.com/protocol-buffers/docs/style) 來規範內容的大小寫，底線，camma 等