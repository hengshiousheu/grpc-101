# gRPC toy examples 專案
目的：了解 gRPC 在實務上的用法
<br>
目標：使用同一份 proto 來產生不同語言之服務，未來要持續測試在 microservices 之下的 gRPC 用法，例如壓力測試、gateway、design pattern等

## 檔案結構
- protos: 存放定義好的 Entity 所在處
- pureJS: 靜態產生 protos(v2) 的 JS 版本，其中實現四種 gRPC 溝通機制
- python: 靜態產生 protos(v2) 的 py 版本，其中實現 Unary gRPC 溝通機制

## 使用說明
開啟 Js 版本 server
```
cd pureJS/server && node index.js
```
開啟 py 版本 client
```
cd python && python3 greet_client.py
```
## 效果呈現
![Qoo](demo/screenshot.png)