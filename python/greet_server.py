from concurrent import futures
import grpc

import greet_pb2
import greet_pb2_grpc

class GreetService(greet_pb2_grpc.GreetServiceServicer):
	"""
	docstring
	"""
	def Greet(self, request, context):
		first_name = request.greeting.first_name
		return greet_pb2.GreetResponse(result='[Python]Hello, %s!' % first_name)

def serve():
	server = grpc.server(futures.ThreadPoolExecutor(max_workers=5))
	greet_pb2_grpc.add_GreetServiceServicer_to_server(GreetService(), server)
	server.add_insecure_port("[::]:50051")
	server.start()
	print("grpc server start...")
	server.wait_for_termination()

if __name__ == '__main__':
	serve()