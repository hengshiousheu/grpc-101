import grpc
import greet_pb2
import greet_pb2_grpc

def run():
	channel = grpc.insecure_channel('localhost:50051')
	stub = greet_pb2_grpc.GreetServiceStub(channel)
	greetingObj = greet_pb2.Greeting()
	greetingObj.first_name = "Hulu"
	greetingObj.last_name = "Netflix"
	response = stub.Greet(greet_pb2.GreetRequest(greeting=greetingObj))
	print("Greeter client received: " + response.result)

if __name__ == "__main__":
	run()