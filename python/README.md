# Python Version 
本資料夾存在 py 版本之 gRPC toy example
## 使用說明

安裝該安裝的
```
python3 install -r requirement.txt
```

編譯 *.proto
```
python3 -m grpc_tools.protoc -I../protos \
--python_out=. \
--grpc_python_out=. \
../protos/greet.proto
```

開啟兩個 terminal，一個使用 server ，一個使用 client
```
python3 greet_server.py
```
```
python3 greet_client.py
```

## 未來改進
Implement other streaming API within gRPC, such as BiDirectional stream.